import bcrypt from 'bcrypt';

/**
 * Function that hashes the string passed as parameter using bcrypt with the default salt(10).
 * @param {string} - String to be hashed.
 * @returns {Promise<string>} - The hashed string.
 */
export async function hash(str: string): Promise<string> {
	const salt = await bcrypt.genSalt();
	return await bcrypt.hash(str, salt);
}

/**
 * Function that checks if the string matches the hash using bycript comparison.
 * @param {string} - The plain text string.
 * @param {string} - The hashed string.
 * @returns {Promise<boolean>} - True if string matches, false otherwise. 
 */
export async function isValidHash(str: string, hash: string): Promise<boolean> {
	return await bcrypt.compare(str, hash);
}
