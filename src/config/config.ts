import dotenv from 'dotenv';

dotenv.config();

export const PORT: number = parseInt(process.env.PORT!);
export const DATABASE_URL: string = process.env.DATABASE_URL!;
export const CLIENT_URL: string = process.env.CLIENT_URL!;
export const CLIENT_SECRET: string = process.env.CLIENT_SECRET!;