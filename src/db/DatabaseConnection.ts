import mongoose from 'mongoose';
import { DATABASE_URL } from '../config/config';

export default class DatabaseConnection {
	private _mongoUrl: string = DATABASE_URL;
	private static _instance: DatabaseConnection;

	private constructor() {}

	public static get Instance() {
		if (this._instance) {
			return this._instance;
		} else {
			this._instance = new DatabaseConnection();
			return this._instance;
		}
	}

	public async connect() {
		try {
			await mongoose.connect(this._mongoUrl, {
				useNewUrlParser: true,
				useFindAndModify: false,
				useCreateIndex: true,
				useUnifiedTopology: true
			});

			let connection: mongoose.Connection = mongoose.connection;

			console.log(`Connected to ${this._mongoUrl}!`);
		} catch (err) {
			throw new Error(`Failed to connect to ${this._mongoUrl}. See more details: ${err}`);
		}
	}

	public disconnect() {
		mongoose.disconnect();
	}
}
