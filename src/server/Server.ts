import session from 'express-session';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import '../security/passport';
import routes from '../routes/routes';
import { PORT, CLIENT_SECRET, CLIENT_URL } from '../config/config';

export default class Server {
	private _port: number;
	private _app: express.Application;

	public constructor(app: express.Application, port: number = PORT) {
		this._port = port;
		this._app = app;

		this.configApp();
		this.setRoutes();

		this.startServer();
	}

	private startServer() {
		this._app.listen(this._port, () => {
			console.log(`Server listening at http://localhost:${this._port}!`);
		});
	}

	private configApp() {
		//cofigure cors
		this._app.use(
			cors({
				origin: CLIENT_URL,
				methods: 'GET, HEAD, PUT, PATCH, POST, DELETE',
				allowedHeaders: 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
				preflightContinue: false,
				optionsSuccessStatus: 204
			})
		);

		//parse application/x-www-form-urlencoded from body
		this._app.use(bodyParser.json());

		//parse application.json from body
		this._app.use(bodyParser.urlencoded({ extended: false }));

		this._app.use(
			session({
				secret: CLIENT_SECRET,
				resave: true,
				saveUninitialized: true
			})
		);
		this._app.use(passport.initialize());
		this._app.use(passport.session());
	}

	private setRoutes() {
		routes(this._app, passport);
	}
}
