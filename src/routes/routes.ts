import express from 'express';
import * as HttpStatus from 'http-status-codes';
import validator from 'validator';
import * as jwt from 'jsonwebtoken';
import { CLIENT_SECRET } from '../config/config';
import UserService from '../services/UserService';
import * as utilities from '../helpers/utilities';
import * as passport from '../security/passport';
import EmotionAnalyticsService from '../services/EmotionAnalyticsService';
import SecurityQuestionService from '../services/SecurityQuestionService';
import QuizQuestionService from '../services/QuizQuestionService';
import { MIN_PASSWORD_LENGTH } from '../helpers/common/Constants';

export default (app: express.Application, passport?: any) => {
	/**
   * Main page route.
   */
	app.get('/', (req: express.Request, res: express.Response) => {
		res.status(HttpStatus.OK).json({
			message: 'Interview Simulator app is working!',
			status: HttpStatus.getStatusText(HttpStatus.OK)
		});
	});

	/**
   * Log in existing user using JWT strategy.
   */
	app.post(
		'/login',
		passport.authenticate('local', {
			session: false
		}),
		(req: express.Request, res: express.Response) => {
			if (req.user) {
				let payload = {
					username: (req.user as any).username,
					email: (req.user as any).email,
					lastUpdated: (req.user as any).lastUpdated
				};

				let token = jwt.sign(payload, CLIENT_SECRET, { expiresIn: '1h' });

				res.status(HttpStatus.OK).json({
					message: 'Login sucessful!',
					token: token,
					status: HttpStatus.getStatusText(HttpStatus.OK)
				});
			} else {
				res.status(HttpStatus.UNAUTHORIZED).json({
					message: 'Login failed!',
					status: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
				});
			}
		}
	);

	/**
	* Register new user and verify credentials.
	*/
	app.post('/register', async (req: express.Request, res: express.Response) => {
		try {
			// check for credeantials existance
			if (
				req.body.username &&
				req.body.password &&
				req.body.email &&
				req.body.securityQuestion &&
				req.body.securityQuestionAnswer
			) {
				// validate email format
				if (validator.isEmail(req.body.email)) {
					//validate password length
					if (req.body.password.length >= MIN_PASSWORD_LENGTH) {
						//check if username already exists
						if (await UserService.Instance.getUserByUsername(req.body.username)) {
							res.status(HttpStatus.NOT_ACCEPTABLE).json({
								message: 'Error! Username already exists!',
								status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
							});
							//check if email already exists
						} else if (await UserService.Instance.getUserByEmail(req.body.email)) {
							res.status(HttpStatus.NOT_ACCEPTABLE).json({
								message: 'Error! Email already exists!',
								status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
							});
						} else {
							await UserService.Instance.addNewUser(
								req.body.username,
								req.body.password,
								req.body.email,
								req.body.securityQuestion,
								req.body.securityQuestionAnswer
							);
							res.status(HttpStatus.OK).json({
								message: 'Account sucessfully created!',
								status: HttpStatus.getStatusText(HttpStatus.OK)
							});
						}
					} else {
						res.status(HttpStatus.NOT_ACCEPTABLE).json({
							message: 'Invalid password length!',
							status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
						});
					}
				} else {
					res.status(HttpStatus.NOT_ACCEPTABLE).json({
						message: 'Invalid email address!',
						status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
					});
				}
			} else {
				res.status(HttpStatus.PRECONDITION_FAILED).json({
					message: 'Please fill in the required fileds!',
					status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
				});
			}
		} catch (e) {
			console.log(e);
			res.status(HttpStatus.PRECONDITION_FAILED).json({
				message: 'Error! Cannot create account!',
				status: HttpStatus.getStatusText(HttpStatus.PRECONDITION_FAILED)
			});
		}
	});

	/**
	* Recover password using secret question.
	*/
	app.post('/recover', async (req: express.Request, res: express.Response) => {
		try {
			// check email format
			if (validator.isEmail(req.body.email)) {
				let user = await UserService.Instance.getUserByEmail(req.body.email);

				// check if user exists
				if (user) {
					// check if the new password matches the old one
					if (await utilities.isValidHash(req.body.newPassword, user.password)) {
						res.status(HttpStatus.NOT_ACCEPTABLE).json({
							message: 'Error! Old password cannot be new password.',
							status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
						});
					} else {
						let isSuccess = await UserService.Instance.resetPassword(
							req.body.email,
							req.body.securityQuestionAnswer,
							req.body.newPassword
						);

						if (isSuccess) {
							res.status(HttpStatus.OK).json({
								message: 'Success! Password updated.',
								status: HttpStatus.getStatusText(HttpStatus.OK)
							});
						} else {
							res.status(HttpStatus.UNAUTHORIZED).json({
								message: 'Error! Secret question answer is incorrect!',
								status: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
							});
						}
					}
				} else {
					res.status(HttpStatus.UNAUTHORIZED).json({
						message: 'Error! No account found for this email address!',
						status: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
					});
				}
			} else {
				res.status(HttpStatus.NOT_ACCEPTABLE).json({
					message: 'Invalid email address!',
					status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
				});
			}
		} catch (e) {
			console.log(e);
			res.status(HttpStatus.PRECONDITION_FAILED).json({
				message: 'Recover reguest failed!',
				status: HttpStatus.getStatusText(HttpStatus.PRECONDITION_FAILED)
			});
		}
	});

	/**
	* Get emotion analytics in the db.
	*/
	app.get(
		'/analytics',
		passport.authenticate('jwt', { session: false }),
		async (req: express.Request, res: express.Response) => {
			try {
				let emotions = await EmotionAnalyticsService.Instance.getAll();
				if (emotions.length > 0) {
					res.send(emotions);
				} else {
					res.status(HttpStatus.NOT_FOUND).json({
						message: 'No analytics found.',
						status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
					});
				}
			} catch (e) {
				console.log(e);
				res.status(HttpStatus.NOT_FOUND).json({
					message: 'No analytics found.',
					status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
				});
			}
		}
	);

	/**
	* Get analytics by interview.
	*/
	app.post(
		'/analyticsByInterview',
		passport.authenticate('jwt', { session: false }),
		async (req: express.Request, res: express.Response) => {
			try {
				let analytics = await EmotionAnalyticsService.Instance.getEmotionAnalyticsByInterview(
					parseFloat(req.body.interviewId)
				);

				if (analytics && analytics.length > 0) {
					res.send(analytics);
				} else {
					res.status(HttpStatus.NOT_FOUND).json({
						message: 'No analytics found.',
						status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
					});
				}
			} catch (e) {
				console.log(e);
				res.status(HttpStatus.NOT_FOUND).json({
					message: 'No analytics found.',
					status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
				});
			}
		}
	);

	/**
	* Get analytics by user.
	*/
	app.post(
		'/analyticsByUser',
		passport.authenticate('jwt', { session: false }),
		async (req: express.Request, res: express.Response) => {
			try {
				let analytics = await EmotionAnalyticsService.Instance.getEmotionAnalyticsByUser(req.body.username);

				if (analytics && analytics.length > 0) {
					res.send(analytics);
				} else {
					res.status(HttpStatus.NOT_FOUND).json({
						message: 'No analytics found.',
						status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
					});
				}
			} catch (e) {
				console.log(e);
				res.status(HttpStatus.NOT_FOUND).json({
					message: 'No analytics found.',
					status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
				});
			}
		}
	);

	/**
	* Add new object to Emotion Analytics.
	*/
	app.post(
		'/addEmotionAnalytics',
		passport.authenticate('jwt', { session: false }),
		async (req: express.Request, res: express.Response) => {
			try {
				if (req.body.interviewId && req.body.username && req.body.feedback) {
					await EmotionAnalyticsService.Instance.addEmotionAnalytics(
						parseFloat(req.body.interviewId),
						req.body.username,
						req.body.feedback
					);

					res.status(HttpStatus.OK).json({
						message: 'Success! Emotion added!',
						status: HttpStatus.getStatusText(HttpStatus.OK)
					});
				} else {
					res.status(HttpStatus.PRECONDITION_FAILED).json({
						message: 'Error! Please specify all the required fields!',
						status: HttpStatus.getStatusText(HttpStatus.PRECONDITION_FAILED)
					});
				}
			} catch (e) {
				console.log(e);
				res.status(HttpStatus.PRECONDITION_FAILED).json({
					message: 'Error! Cannot add emotion.',
					status: HttpStatus.getStatusText(HttpStatus.PRECONDITION_FAILED)
				});
			}
		}
	);

	/**
	 * Get all security questions from db.
	 */
	app.get('/securityQuestions', async (req: express.Request, res: express.Response) => {
		try {
			let securityQuestions = await SecurityQuestionService.Instance.getAllSecurityQuestions();
			res.send(securityQuestions);
		} catch (e) {
			console.log(e);
			res.status(HttpStatus.NOT_FOUND).json({
				message: 'No security questions found.',
				status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
			});
		}
	});

	/**
	 * Get all quiz questions from db.
	 */
	app.get(
		'/quizQuestions',
		passport.authenticate('jwt', { session: false }),
		async (req: express.Request, res: express.Response) => {
			try {
				let quizQuestions = await QuizQuestionService.Instance.getAllQuizQuestions();
				res.send(quizQuestions);
			} catch (e) {
				console.log(e);
				res.status(HttpStatus.NOT_FOUND).json({
					message: 'No quiz questions found.',
					status: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
				});
			}
		}
	);
};
