import EmotionAnalytics, { IEmotionAnalytics } from '../models/EmotionAnalytics';

export default class EmotionAnalyticsService {
	private static _instance: EmotionAnalyticsService;

	private constructor() {}

	public static get Instance() {
		if (this._instance) {
			return this._instance;
		} else {
			this._instance = new EmotionAnalyticsService();
			return this._instance;
		}
	}

	/**
	 * Get all emotion analytics entiries from db.
	 * @returns {Promise<IEmotionAnalytics[] | IEmotionAnalytics>} The entries from the db.
	 */
	public async getAll() {
		return await EmotionAnalytics.find({}).exec();
	}

	/**
	 * Get emotion analytics entries by interview.
	 * @param interviewId
	 * @returns  {Promise<IEmotionAnalytics} The entry form the db.
	 */
	public async getEmotionAnalyticsByInterview(interviewId: number) {
		return await EmotionAnalytics.find({ interviewId: interviewId }).exec();
	}

	/**
	 * Get emotion analytics entries by user.
	 * @param username
	 * @returns  {Promise<IEmotionAnalytics} The entry form the db.
	 */
	public async getEmotionAnalyticsByUser(username: string) {
		return await EmotionAnalytics.find({ username: username }).exec();
	}

	/**
	 * Add emotion analytics entry to db.
	 * @param interviewId 
	 * @param username 
	 * @param feedback
	 * @returns {Promise<IEmotionAnalytics>} The saved entry. 
	 */
	public async addEmotionAnalytics(interviewId: number, username: string, feedback: string) {
		try {
			let feedbackObj = JSON.parse(feedback);

			let analytics = new EmotionAnalytics({
				interviewId: interviewId,
				username: username,
				feedback: feedbackObj
			});

			analytics.save();
		} catch (err) {
			throw new Error(`Error while adding the emotions analytics object. See more details: ${err}`);
		}
	}

	/**
	 * Delete an emotion analytics entry by user.
	 * @param username 
	 */
	public async deleteEmotionAnalyticsObj(username: string) {
		return await EmotionAnalytics.deleteMany({ username: username });
	}
}
