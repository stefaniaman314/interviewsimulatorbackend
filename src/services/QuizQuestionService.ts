import QuizQuestion from '../models/QuizQuestion';

export default class QuizQuestionService {
	private static _instance: QuizQuestionService;

	private constructor() {}

	public static get Instance() {
		if (this._instance) {
			return this._instance;
		} else {
			this._instance = new QuizQuestionService();
			return this._instance;
		}
	}

	/**
     * @returns {Promise<ISecurityQuestion[]>} all entries of type SecurityQuestion from database.
     */
	public async getAllQuizQuestions() {
		return await QuizQuestion.find({}).exec();
	}

	/**
     * Add new security question to database.
     * @param question
     * @returns {Promise<ISecurityQuestion>} the new question.
     */
	public async addQuizQuestion(question: string) {
		let newQuizQuestion = new QuizQuestion({ question: question });
		return await newQuizQuestion.save();
	}
}
