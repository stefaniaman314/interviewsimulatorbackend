import User from '../models/User';
import SecurityQuestion from '../models/SecurityQuestion';
import * as utilities from '../helpers/utilities';

export default class UserService {
	private static _instance: UserService;

	private constructor() {}

	public static get Instance() {
		if (this._instance) {
			return this._instance;
		} else {
			this._instance = new UserService();
			return this._instance;
		}
	}

	/**
     * Find a user by username in database.
     * @param username 
     * @returns {Promise<IUser | null>} the user if found, or null.
     */
	public async getUserByUsername(username: string) {
		return await User.findOne({ username: username }).exec();
	}

	/**
     * Find a user by email in database.
     * @param email 
     * @returns {Promise<IUser | null>}  the user if found, or null.
     */
	public async getUserByEmail(email: string) {
		return await User.findOne({ email: email }).exec();
	}

	/**
     * Add new user to database with hashed password and security question answer.
     * @param username 
     * @param password 
     * @param email 
     * @param securityQuestion 
     * @param securityQuestionAnswer 
     * @returns {Promise<IUser>} the new user.
     */
	public async addNewUser(
		username: string,
		password: string,
		email: string,
		securityQuestion: string,
		securityQuestionAnswer: string
	) {
		let newUser = new User({
			username: username,
			password: password,
			email: email,
			securityQuestion: new SecurityQuestion({
				question: securityQuestion
			}),
			securityQuestionAnswer: securityQuestionAnswer,
			lastUpdated: Date.now()
		});

		//hash password
		let hashedPassword = await utilities.hash(newUser.password);
		newUser.password = hashedPassword;

		//hash security question answer
		let hashedAnswer = await utilities.hash(newUser.securityQuestionAnswer);
		newUser.securityQuestionAnswer = hashedAnswer;

		return await newUser.save();
	}

	/**
     * Delete an existing user from database where email and username match the parametes.
     * @param email 
     * @param username 
     * @returns boolean acknowledgement. 
     */
	public async deleteUser(email: string, username: string) {
		return await User.deleteMany({ email: email, username: username });
	}

	/**
     * Update user by resetting it's password. 
     * @param email 
     * @param securityQuestion 
     * @param securityQuestionAnswer 
     * @param newPassword 
     * @returns {Promise<boolean>} true if the password was updated, false otherwise.
     */
	public async resetPassword(email: string, securityQuestionAnswer: string, newPassword: string) {
		let newPass = await utilities.hash(newPassword);

		let user = await User.findOne({ email: email });

		if (user) {
			if (await utilities.isValidHash(securityQuestionAnswer, user.securityQuestionAnswer)) {
				user.password = newPass;
				user.lastUpdated = Date.now();
				user.save();
				return true;
			}
		}
		return false;
	}
}
