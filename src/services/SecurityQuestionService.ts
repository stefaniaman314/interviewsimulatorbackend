import SecurityQuestion from '../models/SecurityQuestion';

export default class SecurityQuestionService {
	private static _instance: SecurityQuestionService;

	private constructor() {}

	public static get Instance() {
		if (this._instance) {
			return this._instance;
		} else {
			this._instance = new SecurityQuestionService();
			return this._instance;
		}
	}

	/**
     * @returns {Promise<ISecurityQuestion[]>} all entries of type SecurityQuestion from database.
     */
	public async getAllSecurityQuestions() {
		return await SecurityQuestion.find({}).exec();
	}

	/**
     * Add new security question to database.
     * @param question
     * @returns {Promise<ISecurityQuestion>} the new question.
     */
	public async addSecurityQuestion(question: string) {
		let newSecurityQuestion = new SecurityQuestion({ question: question });
		return await newSecurityQuestion.save();
	}
}
