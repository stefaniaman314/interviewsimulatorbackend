import mongoose, { Schema, Document } from 'mongoose';
import { ISecurityQuestion, SecurityQuestionSchema } from './SecurityQuestion';

interface IUser extends Document {
	username: string;
	password: string;
	email: string;
	securityQuestion: ISecurityQuestion;
	securityQuestionAnswer: string;
	lastUpdated: number;
}

const UserSchema = new Schema(
	{
		username: {
			type: String,
			required: true,
			unique: true
		},
		password: {
			type: String,
			required: true
		},
		email: {
			type: String,
			required: true,
			unique: true
		},
		securityQuestion: {
			type: SecurityQuestionSchema,
			required: true
		},
		securityQuestionAnswer: {
			type: String,
			required: true
		},
		lastUpdated: {
			type: Number,
			required: true
		}
	},
	{ collection: 'User' }
);

export default mongoose.model<IUser>('User', UserSchema);
