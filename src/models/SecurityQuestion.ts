import mongoose, { Schema, Document } from 'mongoose';

export interface ISecurityQuestion extends Document {
	question: string;
}

export const SecurityQuestionSchema = new Schema(
	{
		question: {
			type: String,
			required: true
		}
	},
	{ collection: 'SecurityQuestion' }
);

export default mongoose.model<ISecurityQuestion>('SecurityQuestion', SecurityQuestionSchema);
