import { DEFAULT_EMOTION_COUNT } from '../helpers/common/Constants';

export enum Emotion {
	Anger = 0,
	Disgust = 1,
	Fear = 2,
	Happiness = 3,
	Sadness = 4,
	Surprise = 5,
	Neutral = 6
}

export class EmotionMapper {
	emotionsCounter: number;
	angerCounter: number;
	disgustCounter: number;
	fearCounter: number;
	happinessCounter: number;
	sadnessCounter: number;
	surpriseCounter: number;
	neutralCounter: number;

	constructor() {
		this.emotionsCounter = DEFAULT_EMOTION_COUNT;
		this.angerCounter = DEFAULT_EMOTION_COUNT;
		this.disgustCounter = DEFAULT_EMOTION_COUNT;
		this.fearCounter = DEFAULT_EMOTION_COUNT;
		this.happinessCounter = DEFAULT_EMOTION_COUNT;
		this.sadnessCounter = DEFAULT_EMOTION_COUNT;
		this.surpriseCounter = DEFAULT_EMOTION_COUNT;
		this.neutralCounter = DEFAULT_EMOTION_COUNT;
	}

	public incrementEmotionCounter(emotion: Emotion) {
		this.emotionsCounter++;

		switch (emotion) {
			case Emotion.Anger:
				this.angerCounter++;
				break;

			case Emotion.Disgust:
				this.disgustCounter++;
				break;

			case Emotion.Fear:
				this.fearCounter++;
				break;

			case Emotion.Happiness:
				this.happinessCounter++;
				break;

			case Emotion.Sadness:
				this.sadnessCounter++;
				break;

			case Emotion.Surprise:
				this.surpriseCounter++;
				break;

			case Emotion.Neutral:
				this.neutralCounter++;
				break;

			default:
				break;
		}
	}
}
