import mongoose, { Schema, Document } from "mongoose";

export interface IQuizQuestion extends Document {
  question: string;
}

export const QuizQuestionSchema = new Schema(
  {
    question: {
      type: String,
      required: true
    }
  },
  { collection: "QuizQuestion" }
);

export default mongoose.model<IQuizQuestion>(
  "QuizQuestion",
  QuizQuestionSchema
);