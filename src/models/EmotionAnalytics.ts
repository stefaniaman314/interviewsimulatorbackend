import mongoose, { Schema, Document, Collection } from 'mongoose';
import { QuestionEmotionMapper } from './QuestionEmotionMapper';

export interface IEmotionAnalytics extends Document {
	interviewId: number;
	username: string;
	feedback: QuestionEmotionMapper;
}

export const EmotionAnalyticsSchema = new Schema(
	{
		interviewId: {
			type: Number,
			required: true
		},
		username: {
			type: String,
			required: true
		},
		feedback: {
			type: Object,
			required: true
		}
	},
	{ collection: 'EmotionAnalytics' }
);

export default mongoose.model<IEmotionAnalytics>('EmotionAnalytics', EmotionAnalyticsSchema);
