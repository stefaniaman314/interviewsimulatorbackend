import passport from 'passport';
import passportLocal from 'passport-local';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import validator from 'validator';
import { CLIENT_SECRET } from '../config/config';
import User from '../models/User';
import * as utilities from '../helpers/utilities';

const LocalStrategy = passportLocal.Strategy;

/**
 * Passport strategy for authenticating with a username and password.
 * @returns a user or null.
 */
passport.use(
	new LocalStrategy(
		{
			usernameField: "email",
			passwordField: "password"
		},
		async (email, password, done) => {
			try {
				if (validator.isEmail(email)) {
					let user = await User.findOne({ email: email }).exec();
					if (user && await utilities.isValidHash(password, user.password)) {
						return done(null, user.toObject());
					} else {
						return done(null, false);
					}
				} else {
					return done(null, false);
				}
			} catch {
				return done(null);
			}
		}
	)
);

/**
 * Passport strategy for authenticating with a JSON Web Token.
 * @returns the encoded JWT string or null.
 */
passport.use(
	new JwtStrategy(
		{
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			secretOrKey: CLIENT_SECRET
		},
		async (jwtPayload, done) => {
			try {
				let user = await User.findOne({ email: jwtPayload.email }).exec();
				if (user && user.lastUpdated === jwtPayload.lastUpdated) {
					return done(null, user);
				} else {
					return done(null, false);
				}
			} catch (err) {
				return done(err, false);
			}
		}
	)
);