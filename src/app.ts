import express from 'express';
import Server from './server/Server';
import DatabaseConnection from './db/DatabaseConnection';

const app = express();

try {
	DatabaseConnection.Instance.connect().then(() => {
		new Server(app);
	});
} catch (err) {
	console.error(`Error encountered while connecting to the database. ${err}`);
}

export default app;
