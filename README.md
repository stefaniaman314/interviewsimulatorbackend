# Interview Simulator - partea de server a aplicației
## Cuprins
* [Scurtă descriere](#descriere)
* [Dependențe](#dependențe)
* [Instalare](#instalare)
* [Rularea aplicației](#requirements)

### Scurtă descriere ###
Proiectul de față reprezintă partea de server a aplicației de recunoaștere a emoțiilor faciale în simularea unui interviu. Este un server REST, construit pe platforma Node.js cu framework-ul Express.js, care se folosește de o bază de date de tip MongoDB.

În continuare sunt prezentați pașii care trebuie urmați pentru rularea aplicației.

### Dependențe ###

Pentru ca aplicația să ruleze este necesară instalarea platformei [node.js](http://nodejs.org), împreună cu [npm](http://npmjs.com) (node package manager).

De asemenea, este necesară instalarea versiunii Comunity pentru baza de date MongoDB: [MongoDb Community Server](https://www.mongodb.com/try/download/community).

### Instalare ###
Aplicația va instala dependențele specificate în fișierul `package.json` prin executarea următoarei comenzi, în directorul sursă al proiectului:

```SH
$ npm install
```
### Rularea aplicației ###
Pentru a rula aplicația se poate executa comanda:

```SH
$ npm start
```
Aplicația mai poate fi pornită cu opțiunea `Run Without Debugging` sau `CTRL+F5`.


Odată pornit, server-ul ascultă request-uri de la client în mod continuu, pentru a răspunde la ele. 
